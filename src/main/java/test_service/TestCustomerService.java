/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package test_service;

import com.werapan.databaseproject.model.Customer;
import com.werapan.databaseproject.service.CustomerService;

/**
 *
 * @author ADMIN
 */
public class TestCustomerService {
    public static void main(String[] args) {
        CustomerService cs = new CustomerService();
        for(Customer customer: cs.getCustomers()) {
            System.out.println(customer);        
        }
        System.out.println(cs.getByTel("0812345678"));
        
        Customer cus1 = new Customer("kob","0812345677");
        cs.addNew(cus1);
        System.out.println("After Add");
        for(Customer customer: cs.getCustomers()) {
            System.out.println(customer);        
        }
        
        Customer delCus = cs.getByTel("0812345677");
        delCus.setTel("0812345688");
        cs.update(delCus);
        System.out.println("After Update");
        for(Customer customer: cs.getCustomers()) {
            System.out.println(customer);        
        }
        
        cs.delete(delCus);
        System.out.println("After Delete");
        for(Customer customer: cs.getCustomers()) {
            System.out.println(customer);        
        }
    }
    
}
